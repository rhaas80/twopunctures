(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[      6308,        262]*)
(*NotebookOutlinePosition[      6939,        284]*)
(*  CellTagsIndexPosition[      6895,        280]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(x := \(-b\)*\((1 + 1/4*\((A + 1)\)^2)\)/\((1 - 1/4*\((A + 1)\)^2)\)*2*
        B/\((1 + B^2)\)\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(x\)], "Input"],

Cell[BoxData[
    \(\(-\(\(2\ \((1 + 1\/4\ \((1 + A)\)\^2)\)\ b\ B\)\/\(\((1 - 
                1\/4\ \((1 + A)\)\^2)\)\ \((1 + B\^2)\)\)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[x]\)], "Input"],

Cell["\<\
(-2*(1 + Power(1 + A,2)/4.)*b*B)/((1 - Power(1 + A,2)/4.)*(1 + \
Power(B,2)))\
\>", "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(FullSimplify[D[x, A]]\)], "Input"],

Cell[BoxData[
    \(\(-\(\(32\ \((1 + A)\)\ b\ B\)\/\(\((\(-3\) + 2\ A + A\^2)\)\^2\ \((1 + 
                B\^2)\)\)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[FullSimplify[D[x, A]]]\)], "Input"],

Cell["\<\
(-32*(1 + A)*b*B)/(Power(-3 + 2*A + Power(A,2),2)*(1 + Power(B,2)))\
\
\>", "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(FullSimplify[D[x, B]]\)], "Input"],

Cell[BoxData[
    \(\(-\(\(2\ \((5 + A\ \((2 + A)\))\)\ b\ \((\(-1\) + 
                B\^2)\)\)\/\(\((\(-1\) + A)\)\ \((3 + 
                A)\)\ \((1 + B\^2)\)\^2\)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[FullSimplify[D[x, B]]]\)], "Input"],

Cell["\<\
(-2*(5 + A*(2 + A))*b*(-1 + Power(B,2)))/((-1 + A)*(3 + A)*Power(1 \
+ Power(B,2),2))\
\>", "Output"]
}, Open  ]],

Cell[BoxData[
    \(r := b*\((A + 1)\)/\((1 - 1/4*\((A + 1)\)^2)\)*\((1 - B^2)\)/\((1 + 
              B^2)\)\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(r\)], "Input"],

Cell[BoxData[
    \(\(\((1 + A)\)\ b\ \((1 - B\^2)\)\)\/\(\((1 - 1\/4\ \((1 + A)\)\^2)\)\ \
\((1 + B\^2)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[r]\)], "Input"],

Cell["\<\
((1 + A)*b*(1 - Power(B,2)))/((1 - Power(1 + A,2)/4.)*(1 + \
Power(B,2)))\
\>", "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(FullSimplify[D[r, A]]\)], "Input"],

Cell[BoxData[
    \(\(-\(\(4\ \((5 + A\ \((2 + A)\))\)\ b\ \((\(-1\) + 
                B\^2)\)\)\/\(\((\(-1\) + A)\)\^2\ \((3 + A)\)\^2\ \((1 + 
                B\^2)\)\)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[FullSimplify[D[r, A]]]\)], "Input"],

Cell["\<\
(-4*(5 + A*(2 + A))*b*(-1 + Power(B,2)))/
   (Power(-1 + A,2)*Power(3 + A,2)*(1 + Power(B,2)))\
\>", "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(FullSimplify[D[r, B]]\)], "Input"],

Cell[BoxData[
    \(\(16\ \((1 + A)\)\ b\ B\)\/\(\((\(-3\) + 2\ A + A\^2)\)\ \((1 + B\^2)\)\
\^2\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[FullSimplify[D[r, B]]]\)], "Input"],

Cell["\<\
(16*(1 + A)*b*B)/((-3 + 2*A + Power(A,2))*Power(1 + \
Power(B,2),2))\
\>", "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(J11 = FullSimplify[D[x, A]]\)], "Input"],

Cell[BoxData[
    \(\(-\(\(32\ \((1 + A)\)\ b\ B\)\/\(\((\(-3\) + 2\ A + A\^2)\)\^2\ \((1 + 
                B\^2)\)\)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[FullSimplify[D[J11, A]]]\)], "Input"],

Cell["\<\
(32*(7 + 3*A*(2 + A))*b*B)/(Power(-1 + A,3)*Power(3 + A,3)*(1 + \
Power(B,2)))\
\>", "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[FullSimplify[D[J11, B]]]\)], "Input"],

Cell["\<\
(32*(1 + A)*b*(-1 + Power(B,2)))/(Power(-3 + 2*A + \
Power(A,2),2)*Power(1 + Power(B,2),2))\
\>", "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(J12 = FullSimplify[D[x, B]]\)], "Input"],

Cell[BoxData[
    \(\(-\(\(2\ \((5 + A\ \((2 + A)\))\)\ b\ \((\(-1\) + 
                B\^2)\)\)\/\(\((\(-1\) + A)\)\ \((3 + 
                A)\)\ \((1 + B\^2)\)\^2\)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(J21 = FullSimplify[D[r, A]]\)], "Input"],

Cell[BoxData[
    \(\(-\(\(4\ \((5 + A\ \((2 + A)\))\)\ b\ \((\(-1\) + 
                B\^2)\)\)\/\(\((\(-1\) + A)\)\^2\ \((3 + A)\)\^2\ \((1 + 
                B\^2)\)\)\)\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(J22 = FullSimplify[D[r, B]]\)], "Input"],

Cell[BoxData[
    \(\(16\ \((1 + A)\)\ b\ B\)\/\(\((\(-3\) + 2\ A + A\^2)\)\ \((1 + B\^2)\)\
\^2\)\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(CForm[FullSimplify[D[FullSimplify[D[J22, B]], B]]]\)], "Input"],

Cell["\<\
(192*(1 + A)*b*B*(-1 + Power(B,2)))/((-3 + 2*A + \
Power(A,2))*Power(1 + Power(B,2),4))\
\>", "Output"]
}, Open  ]]
},
FrontEndVersion->"5.2 for X",
ScreenRectangle->{{0, 1280}, {0, 1024}},
WindowSize->{742, 754},
WindowMargins->{{155, Automatic}, {Automatic, 17}}
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 127, 2, 27, "Input"],

Cell[CellGroupData[{
Cell[1906, 57, 34, 1, 27, "Input"],
Cell[1943, 60, 155, 2, 59, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2135, 67, 41, 1, 27, "Input"],
Cell[2179, 70, 103, 3, 27, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2319, 78, 54, 1, 27, "Input"],
Cell[2376, 81, 136, 2, 47, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2549, 88, 61, 1, 27, "Input"],
Cell[2613, 91, 95, 3, 27, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2745, 99, 54, 1, 27, "Input"],
Cell[2802, 102, 186, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[3025, 110, 61, 1, 27, "Input"],
Cell[3089, 113, 111, 3, 27, "Output"]
}, Open  ]],
Cell[3215, 119, 122, 2, 27, "Input"],

Cell[CellGroupData[{
Cell[3362, 125, 34, 1, 27, "Input"],
Cell[3399, 128, 122, 2, 55, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[3558, 135, 41, 1, 27, "Input"],
Cell[3602, 138, 99, 3, 40, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[3738, 146, 54, 1, 27, "Input"],
Cell[3795, 149, 189, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4021, 157, 61, 1, 27, "Input"],
Cell[4085, 160, 120, 3, 55, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4242, 168, 54, 1, 27, "Input"],
Cell[4299, 171, 112, 2, 47, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4448, 178, 61, 1, 27, "Input"],
Cell[4512, 181, 94, 3, 40, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4643, 189, 60, 1, 27, "Input"],
Cell[4706, 192, 136, 2, 47, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4879, 199, 63, 1, 27, "Input"],
Cell[4945, 202, 104, 3, 40, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[5086, 210, 63, 1, 27, "Input"],
Cell[5152, 213, 117, 3, 40, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[5306, 221, 60, 1, 27, "Input"],
Cell[5369, 224, 186, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[5592, 232, 60, 1, 27, "Input"],
Cell[5655, 235, 189, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[5881, 243, 60, 1, 27, "Input"],
Cell[5944, 246, 112, 2, 47, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[6093, 253, 83, 1, 27, "Input"],
Cell[6179, 256, 113, 3, 40, "Output"]
}, Open  ]]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

