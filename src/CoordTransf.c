/* TwoPunctures:  File  "CoordTransf.c"*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <time.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <assert.h>
#include "cctk_Parameters.h"
#include "TP_utilities.h"
#include "TwoPunctures.h"

/*-----------------------------------------------------------*/
void
AB_To_XR (int nvar, CCTK_REAL A, CCTK_REAL B, CCTK_REAL *X, CCTK_REAL *R,
	  derivs U)
/* On Entrance: U.d0[]=U[]; U.d1[] =U[]_A;  U.d2[] =U[]_B;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_AA; U.d12[]=U[]_AB; U.d13[]=U[]_A3; */
/*                          U.d22[]=U[]_BB; U.d23[]=U[]_B3; U.d33[]=U[]_33; */
/* At Exit:     U.d0[]=U[]; U.d1[] =U[]_X;  U.d2[] =U[]_R;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_XX; U.d12[]=U[]_XR; U.d13[]=U[]_X3; */
/*                          U.d22[]=U[]_RR; U.d23[]=U[]_R3; U.d33[]=U[]_33; */
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL At = 0.5 * (A + 1), A_X, A_XX, B_R, B_RR;
  int ivar;

  *X = 2 * atanh (At);
  *R = Pih + 2 * atan (B);

  A_X = 1 - At * At;
  A_XX = -At * A_X;
  B_R = 0.5 * (1 + B * B);
  B_RR = B * B_R;

  for (ivar = 0; ivar < nvar; ivar++)
  {
    U.d11[ivar] = A_X * A_X * U.d11[ivar] + A_XX * U.d1[ivar];
    U.d12[ivar] = A_X * B_R * U.d12[ivar];
    U.d13[ivar] = A_X * U.d13[ivar];
    U.d22[ivar] = B_R * B_R * U.d22[ivar] + B_RR * U.d2[ivar];
    U.d23[ivar] = B_R * U.d23[ivar];
    U.d1[ivar] = A_X * U.d1[ivar];
    U.d2[ivar] = B_R * U.d2[ivar];
  }
}

/*-----------------------------------------------------------*/
void
C_To_c (int nvar, CCTK_REAL X, CCTK_REAL R, CCTK_REAL *x, CCTK_REAL *r,
	derivs U)
/* On Entrance: U.d0[]=U[]; U.d1[] =U[]_X;  U.d2[] =U[]_R;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_XX; U.d12[]=U[]_XR; U.d13[]=U[]_X3; */
/*                          U.d22[]=U[]_RR; U.d23[]=U[]_R3; U.d33[]=U[]_33; */
/* At Exit:     U.d0[]=U[]; U.d1[] =U[]_x;  U.d2[] =U[]_r;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_xx; U.d12[]=U[]_xr; U.d13[]=U[]_x3; */
/*                          U.d22[]=U[]_rr; U.d23[]=U[]_r3; U.d33[]=U[]_33; */
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL C_c2, U_cb, U_CB;
  gsl_complex C, C_c, C_cc, c, c_C, c_CC, U_c, U_cc, U_C, U_CC;
  int ivar;

  C = gsl_complex_rect (X, R);

  c = gsl_complex_mul_real (gsl_complex_cosh (C), par_b);	/* c=b*cosh(C)*/
  c_C = gsl_complex_mul_real (gsl_complex_sinh (C), par_b);
  c_CC = c;

  C_c = gsl_complex_inverse (c_C);
  C_cc = gsl_complex_negative (gsl_complex_mul (gsl_complex_mul (C_c, C_c), gsl_complex_mul (C_c, c_CC)));
  C_c2 = gsl_complex_abs2 (C_c);

  for (ivar = 0; ivar < nvar; ivar++)
  {
    /* U_C = 0.5*(U_X3-i*U_R3)*/
    /* U_c = U_C*C_c = 0.5*(U_x3-i*U_r3)*/
    U_C = gsl_complex_rect (0.5 * U.d13[ivar], -0.5 * U.d23[ivar]);
    U_c = gsl_complex_mul (U_C, C_c);
    U.d13[ivar] = 2. * GSL_REAL(U_c);
    U.d23[ivar] = -2. * GSL_IMAG(U_c);

    /* U_C = 0.5*(U_X-i*U_R)*/
    /* U_c = U_C*C_c = 0.5*(U_x-i*U_r)*/
    U_C = gsl_complex_rect (0.5 * U.d1[ivar], -0.5 * U.d2[ivar]);
    U_c = gsl_complex_mul (U_C, C_c);
    U.d1[ivar] = 2. * GSL_REAL(U_c);
    U.d2[ivar] = -2. * GSL_IMAG(U_c);

    /* U_CC = 0.25*(U_XX-U_RR-2*i*U_XR)*/
    /* U_CB = d^2(U)/(dC*d\bar{C}) = 0.25*(U_XX+U_RR)*/
    U_CC = gsl_complex_rect (0.25 * (U.d11[ivar] - U.d22[ivar]), -0.5 * U.d12[ivar]);
    U_CB = 0.25 * (U.d11[ivar] + U.d22[ivar]);

    /* U_cc = C_cc*U_C+(C_c)^2*U_CC*/
    U_cb = U_CB * C_c2;
    U_cc = gsl_complex_add (gsl_complex_mul (C_cc, U_C), gsl_complex_mul (gsl_complex_mul (C_c, C_c), U_CC));

    /* U_xx = 2*(U_cb+Re[U_cc])*/
    /* U_rr = 2*(U_cb-Re[U_cc])*/
    /* U_rx = -2*Im[U_cc]*/
    U.d11[ivar] = 2 * (U_cb + GSL_REAL(U_cc));
    U.d22[ivar] = 2 * (U_cb - GSL_REAL(U_cc));
    U.d12[ivar] = -2 * GSL_IMAG(U_cc);
  }

  *x = GSL_REAL(c);
  *r = GSL_IMAG(c);
}

/*-----------------------------------------------------------*/
void
save_C_To_c (int nvar, CCTK_REAL X, CCTK_REAL R, CCTK_REAL *x, CCTK_REAL *r,
	derivs U)
/* On Entrance: U.d0[]=U[]; U.d1[] =U[]_X;  U.d2[] =U[]_R;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_XX; U.d12[]=U[]_XR; U.d13[]=U[]_X3; */
/*                          U.d22[]=U[]_RR; U.d23[]=U[]_R3; U.d33[]=U[]_33; */
/* At Exit:     U.d0[]=U[]; U.d1[] =U[]_x;  U.d2[] =U[]_r;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_xx; U.d12[]=U[]_xr; U.d13[]=U[]_x3; */
/*                          U.d22[]=U[]_rr; U.d23[]=U[]_r3; U.d33[]=U[]_33; */
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL C_c2, U_cb, U_CB;
  gsl_complex C, C_c, C_cc, c, c_C, c_CC, U_c, U_cc, U_C, U_CC;
  int ivar;

  C = gsl_complex_rect (X, R);

  c = gsl_complex_mul_real (gsl_complex_cosh (C), par_b); /* c=b*cosh(C)*/
  c_C = gsl_complex_mul_real (gsl_complex_sinh (C), par_b);
  c_CC = c;

  C_c = gsl_complex_inverse (c_C);
  if (((fabs(R)<1.e-100) || (fabs(R-Pi)<1.e-100)) && (fabs(X)<1.e-100))
    C_c = gsl_complex_rect (0.0, 0.0);
  C_cc = gsl_complex_negative (gsl_complex_mul (gsl_complex_mul (C_c, C_c), gsl_complex_mul (C_c, c_CC)));
  C_c2 = gsl_complex_abs2 (C_c);

  for (ivar = 0; ivar < nvar; ivar++)
  {
    /* U_C = 0.5*(U_X3-i*U_R3)*/
    /* U_c = U_C*C_c = 0.5*(U_x3-i*U_r3)*/
    U_C = gsl_complex_rect (0.5 * U.d13[ivar], -0.5 * U.d23[ivar]);
    U_c = gsl_complex_mul (U_C, C_c);
    U.d13[ivar] = 2. * GSL_REAL(U_c);
    U.d23[ivar] = -2. * GSL_IMAG(U_c);

    /* U_C = 0.5*(U_X-i*U_R)*/
    /* U_c = U_C*C_c = 0.5*(U_x-i*U_r)*/
    U_C = gsl_complex_rect (0.5 * U.d1[ivar], -0.5 * U.d2[ivar]);
    U_c = gsl_complex_mul (U_C, C_c);
    U.d1[ivar] = 2. * GSL_REAL(U_c);
    U.d2[ivar] = -2. * GSL_IMAG(U_c);
#if 0
    if (fabs(U.d2[ivar]) > 1000)
      printf("%g (X,R) %g %g (x,r) %g %g (.r,.i) %g %g \n",
             U.d2[ivar], X, R, c.r, c.i ,C_c.r, C_c.i);
#endif

    /* U_CC = 0.25*(U_XX-U_RR-2*i*U_XR)*/
    /* U_CB = d^2(U)/(dC*d\bar{C}) = 0.25*(U_XX+U_RR)*/
    U_CC = gsl_complex_rect (0.25 * (U.d11[ivar] - U.d22[ivar]), -0.5 * U.d12[ivar]);
    U_CB = 0.25 * (U.d11[ivar] + U.d22[ivar]);

    /* U_cc = C_cc*U_C+(C_c)^2*U_CC*/
    U_cb = U_CB * C_c2;
    U_cc = gsl_complex_add (gsl_complex_mul (C_cc, U_C), gsl_complex_mul (gsl_complex_mul (C_c, C_c), U_CC));

    /* U_xx = 2*(U_cb+Re[U_cc])*/
    /* U_rr = 2*(U_cb-Re[U_cc])*/
    /* U_rx = -2*Im[U_cc]*/
    U.d11[ivar] = 2 * (U_cb + GSL_REAL(U_cc));
    U.d22[ivar] = 2 * (U_cb - GSL_REAL(U_cc));
    U.d12[ivar] = -2 * GSL_IMAG(U_cc);

    if (R<1.e-100)
    {
      U.d13[ivar] = 0.0;
      U.d23[ivar] = 0.0;
    }
  }

  *x = GSL_REAL(c);
  *r = GSL_IMAG(c);
}
/*-----------------------------------------------------------*/
void
rx3_To_xyz (int nvar, CCTK_REAL x, CCTK_REAL r, CCTK_REAL phi,
	    CCTK_REAL *y, CCTK_REAL *z, derivs U)
/* On Entrance: U.d0[]=U[]; U.d1[] =U[]_x;  U.d2[] =U[]_r;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_xx; U.d12[]=U[]_xr; U.d13[]=U[]_x3; */
/*                          U.d22[]=U[]_rr; U.d23[]=U[]_r3; U.d33[]=U[]_33; */
/* At Exit:     U.d0[]=U[]; U.d1[] =U[]_x;  U.d2[] =U[]_y;  U.dz[] =U[]_z;  */
/*                          U.d11[]=U[]_xx; U.d12[]=U[]_xy; U.d1z[]=U[]_xz; */
/*                          U.d22[]=U[]_yy; U.d2z[]=U[]_yz; U.dzz[]=U[]_zz; */
{
  int jvar;
  CCTK_REAL
    sin_phi = sin (phi),
    cos_phi = cos (phi),
    sin2_phi = sin_phi * sin_phi,
    cos2_phi = cos_phi * cos_phi,
    sin_2phi = 2 * sin_phi * cos_phi,
    cos_2phi = cos2_phi - sin2_phi, r_inv = 1 / r, r_inv2 = r_inv * r_inv;

  *y = r * cos_phi;
  *z = r * sin_phi;

  for (jvar = 0; jvar < nvar; jvar++)
  {
    CCTK_REAL U_x = U.d1[jvar], U_r = U.d2[jvar], U_3 = U.d3[jvar],
      U_xx = U.d11[jvar], U_xr = U.d12[jvar], U_x3 = U.d13[jvar],
      U_rr = U.d22[jvar], U_r3 = U.d23[jvar], U_33 = U.d33[jvar];
    U.d1[jvar] = U_x;		/* U_x*/
    U.d2[jvar] = U_r * cos_phi - U_3 * r_inv * sin_phi;	/* U_y*/
    U.d3[jvar] = U_r * sin_phi + U_3 * r_inv * cos_phi;	/* U_z*/
    U.d11[jvar] = U_xx;		/* U_xx*/
    U.d12[jvar] = U_xr * cos_phi - U_x3 * r_inv * sin_phi;	/* U_xy*/
    U.d13[jvar] = U_xr * sin_phi + U_x3 * r_inv * cos_phi;	/* U_xz*/
    U.d22[jvar] = U_rr * cos2_phi + r_inv2 * sin2_phi * (U_33 + r * U_r)	/* U_yy*/
      + sin_2phi * r_inv2 * (U_3 - r * U_r3);
    U.d23[jvar] = 0.5 * sin_2phi * (U_rr - r_inv * U_r - r_inv2 * U_33)	/* U_yz*/
      - cos_2phi * r_inv2 * (U_3 - r * U_r3);
    U.d33[jvar] = U_rr * sin2_phi + r_inv2 * cos2_phi * (U_33 + r * U_r)	/* U_zz*/
      - sin_2phi * r_inv2 * (U_3 - r * U_r3);
  }
}

/*-----------------------------------------------------------*/
/* same as above, but with check for r==0 */
void
save_rx3_To_xyz (int nvar, CCTK_REAL x, CCTK_REAL r, CCTK_REAL phi,
	    CCTK_REAL *y, CCTK_REAL *z, derivs U)
/* On Entrance: U.d0[]=U[]; U.d1[] =U[]_x;  U.d2[] =U[]_r;  U.d3[] =U[]_3;  */
/*                          U.d11[]=U[]_xx; U.d12[]=U[]_xr; U.d13[]=U[]_x3; */
/*                          U.d22[]=U[]_rr; U.d23[]=U[]_r3; U.d33[]=U[]_33; */
/* At Exit:     U.d0[]=U[]; U.d1[] =U[]_x;  U.d2[] =U[]_y;  U.dz[] =U[]_z;  */
/*                          U.d11[]=U[]_xx; U.d12[]=U[]_xy; U.d1z[]=U[]_xz; */
/*                          U.d22[]=U[]_yy; U.d2z[]=U[]_yz; U.dzz[]=U[]_zz; */
{
  int jvar;
  CCTK_REAL
    sin_phi = sin (phi),
    cos_phi = cos (phi),
    sin2_phi = sin_phi * sin_phi,
    cos2_phi = cos_phi * cos_phi,
    sin_2phi = 2 * sin_phi * cos_phi,
    cos_2phi = cos2_phi - sin2_phi, r_inv, r_inv2;
#if 1
  if (r < 1.e-10)
    r_inv = 0;
  else
    r_inv = 1 / r;
#else
  if (r < 1.e-100)
    r = 1.e-100;
  r_inv = 1 / r;
#endif
  r_inv2 = r_inv * r_inv;

  *y = r * cos_phi;
  *z = r * sin_phi;

  for (jvar = 0; jvar < nvar; jvar++)
  {
    CCTK_REAL U_x = U.d1[jvar], U_r = U.d2[jvar], U_3 = U.d3[jvar],
      U_xx = U.d11[jvar], U_xr = U.d12[jvar], U_x3 = U.d13[jvar],
      U_rr = U.d22[jvar], U_r3 = U.d23[jvar], U_33 = U.d33[jvar];
    /*
    if (r < 1.1e-10)
    {
      U_3 = 0.0;
      U_x3 = 0.0;
      U_r3 = 0.0;
      U_33 = 0.0;
    }
    */
    U.d1[jvar] = U_x;		/* U_x*/
    U.d11[jvar] = U_xx;		/* U_xx*/
    U.d2[jvar] = U_r * cos_phi - U_3 * r_inv * sin_phi;	/* U_y*/
    U.d3[jvar] = U_r * sin_phi + U_3 * r_inv * cos_phi;	/* U_z*/
    U.d12[jvar] = U_xr * cos_phi - U_x3 * r_inv * sin_phi;	/* U_xy*/
    U.d13[jvar] = U_xr * sin_phi + U_x3 * r_inv * cos_phi;	/* U_xz*/
    U.d22[jvar] = U_rr * cos2_phi + r_inv2 * sin2_phi * (U_33 + r * U_r)	/* U_yy*/
      + sin_2phi * r_inv2 * (U_3 - r * U_r3);
    U.d23[jvar] = 0.5 * sin_2phi * (U_rr - r_inv * U_r - r_inv2 * U_33)	/* U_yz*/
      - cos_2phi * r_inv2 * (U_3 - r * U_r3);
    U.d33[jvar] = U_rr * sin2_phi + r_inv2 * cos2_phi * (U_33 + r * U_r)	/* U_zz*/
      - sin_2phi * r_inv2 * (U_3 - r * U_r3);
  }
}

/*-----------------------------------------------------------*/

#include "coords.c"

static inline void copy_derivs(int nvar, derivs a, derivs b)
{
    for (int i=0; i<nvar; i++)
    {
        b.d0[i] = a.d0[i];
        b.d1[i] = a.d1[i];
        b.d2[i] = a.d2[i];
        b.d3[i] = a.d3[i];
        b.d11[i]= a.d11[i];
        b.d12[i]= a.d12[i];
        b.d13[i]= a.d13[i];
        b.d22[i]= a.d22[i];
        b.d23[i]= a.d23[i];
        b.d33[i]= a.d33[i];
    }
}
static inline void zero_derivs(int nvar, derivs b)
{
    for (int i=0; i<nvar; i++)
    {
        b.d0[i] = 0.0;
        b.d1[i] = 0.0;
        b.d2[i] = 0.0;
        b.d3[i] = 0.0;
        b.d11[i]= 0.0;
        b.d12[i]= 0.0;
        b.d13[i]= 0.0;
        b.d22[i]= 0.0;
        b.d23[i]= 0.0;
        b.d33[i]= 0.0;
    }
}

void
AB3_To_xr3 (int nvar, CCTK_REAL  A, CCTK_REAL  B,
                      CCTK_REAL *x, CCTK_REAL *r, derivs U)
{
    DECLARE_CCTK_PARAMETERS;
    derivs tmp_U;
    allocate_derivs(&tmp_U, nvar);
    zero_derivs(nvar, tmp_U);
    *x = -par_b * (1.+0.25*(A+1.)*(A+1.))/(1.-0.25*(A+1.)*(A+1.)) *
                  2*B    /(1+B*B);
    *r =  par_b * (A+1.)                 /(1.-0.25*(A+1.)*(A+1.)) *
                  (1-B*B)/(1+B*B);
    /* transformation of a scalar for ivar==0 and for a vector for
       ivar in [1,3] */
#define bAB par_b, A, B
#define bxr par_b, *x, *r
    /* data itself does not change */
    tmp_U.d0[0] = U.d0[0];
    /* However, the derivatives do */
    for (int i=1; i<=3; i++)
    {
        /* first, the translation of an scalar */
        tmp_U.d1[0] += Jm(i,1,bxr)*get_U_(U, i)[0];
        tmp_U.d2[0] += Jm(i,2,bxr)*get_U_(U, i)[0];
        tmp_U.d3[0] += Jm(i,3,bxr)*get_U_(U, i)[0];
        for (int j=1; j<=3; j++)
        {
            tmp_U.d11[0] += Jm(i,1,bxr)*Jm(j,1,bxr)*get_U__(U,i,j)[0];
            tmp_U.d12[0] += Jm(i,1,bxr)*Jm(j,2,bxr)*get_U__(U,i,j)[0];
            tmp_U.d13[0] += Jm(i,1,bxr)*Jm(j,3,bxr)*get_U__(U,i,j)[0];
            tmp_U.d22[0] += Jm(i,2,bxr)*Jm(j,2,bxr)*get_U__(U,i,j)[0];
            tmp_U.d23[0] += Jm(i,2,bxr)*Jm(j,3,bxr)*get_U__(U,i,j)[0];
            tmp_U.d33[0] += Jm(i,3,bxr)*Jm(j,3,bxr)*get_U__(U,i,j)[0];
            /* from here on, the vector translation */
            if (nvar>1)
            {
                tmp_U.d0[j] += J(j,i,bAB)*U.d0[i];
                for (int k=1; k<=3; k++)
                {
                    tmp_U.d1[k] = Jm(i,1,bxr) * (J_(k,j,i,bAB) * U.d0[j] +
                                                 J(k,j,bAB) * get_U_(U, i)[j] );
                    tmp_U.d2[k] = Jm(i,2,bxr) * (J_(k,j,i,bAB) * U.d0[j] +
                                                 J(k,j,bAB) * get_U_(U, i)[j] );
                    tmp_U.d3[k] = Jm(i,3,bxr) * (J_(k,j,i,bAB) * U.d0[j] +
                                                 J(k,j,bAB) * get_U_(U, i)[j] );
                    for (int l=1; l<=3; l++)
                    {
                        tmp_U.d11[l]+= Jm_(i,1,1,bxr) *
                                       ( J_(l,j,i,bAB)*U.d0[j] +
                                         J(l,j,bAB)*get_U_(U, j)[i] ) +
                                       Jm(i,1,bxr)*Jm(k,1,bxr) *
                                       ( J__(l,j,k,i,bAB)*U.d0[j] +
                                         J_(l,j,i,bAB)*get_U_(U, k)[j] +
                                         J_(l,j,k,bAB)*get_U_(U, i)[j] +
                                         J(l,j,bAB)*get_U__(U,k,i)[j] );
                        tmp_U.d12[l]+= Jm_(i,1,2,bxr) *
                                       ( J_(l,j,i,bAB)*U.d0[j] +
                                         J(l,j,bAB)*get_U_(U, j)[i] ) +
                                       Jm(i,1,bxr)*Jm(k,2,bxr) *
                                       ( J__(l,j,k,i,bAB)*U.d0[j] +
                                         J_(l,j,i,bAB)*get_U_(U, k)[j] +
                                         J_(l,j,k,bAB)*get_U_(U, i)[j] +
                                         J(l,j,bAB)*get_U__(U,k,i)[j] );
                        tmp_U.d13[l]+= Jm_(i,1,3,bxr) *
                                       ( J_(l,j,i,bAB)*U.d0[j] +
                                         J(l,j,bAB)*get_U_(U, j)[i] ) +
                                       Jm(i,1,bxr)*Jm(k,3,bxr) *
                                       ( J__(l,j,k,i,bAB)*U.d0[j] +
                                         J_(l,j,i,bAB)*get_U_(U, k)[j] +
                                         J_(l,j,k,bAB)*get_U_(U, i)[j] +
                                         J(l,j,bAB)*get_U__(U,k,i)[j] );
                        tmp_U.d22[l]+= Jm_(i,2,2,bxr) *
                                       ( J_(l,j,i,bAB)*U.d0[j] +
                                         J(l,j,bAB)*get_U_(U, j)[i] ) +
                                       Jm(i,2,bxr)*Jm(k,2,bxr) *
                                       ( J__(l,j,k,i,bAB)*U.d0[j] +
                                         J_(l,j,i,bAB)*get_U_(U, k)[j] +
                                         J_(l,j,k,bAB)*get_U_(U, i)[j] +
                                         J(l,j,bAB)*get_U__(U,k,i)[j] );
                        tmp_U.d23[l]+= Jm_(i,2,3,bxr) *
                                       ( J_(l,j,i,bAB)*U.d0[j] +
                                         J(l,j,bAB)*get_U_(U, j)[i] ) +
                                       Jm(i,2,bxr)*Jm(k,3,bxr) *
                                       ( J__(l,j,k,i,bAB)*U.d0[j] +
                                         J_(l,j,i,bAB)*get_U_(U, k)[j] +
                                         J_(l,j,k,bAB)*get_U_(U, i)[j] +
                                         J(l,j,bAB)*get_U__(U,k,i)[j] );
                        tmp_U.d33[l]+= Jm_(i,3,3,bxr) *
                                       ( J_(l,j,i,bAB)*U.d0[j] +
                                         J(l,j,bAB)*get_U_(U, j)[i] ) +
                                       Jm(i,3,bxr)*Jm(k,3,bxr) *
                                       ( J__(l,j,k,i,bAB)*U.d0[j] +
                                         J_(l,j,i,bAB)*get_U_(U, k)[j] +
                                         J_(l,j,k,bAB)*get_U_(U, i)[j] +
                                         J(l,j,bAB)*get_U__(U,k,i)[j] );
                    }
                }
            }
        }
    }
#undef bxr
#undef bAB
    copy_derivs(nvar, tmp_U, U);
    free_derivs(&tmp_U, nvar);
}

void
xr3_To_AB3 (int nvar, CCTK_REAL  x, CCTK_REAL  r,
                      CCTK_REAL *A, CCTK_REAL *B, derivs U)
{
    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL r_2, aux1, aux2, tmp1, tmp2;
    derivs tmp_U;
    allocate_derivs(&tmp_U, nvar);
    zero_derivs(nvar, tmp_U);
    r_2 = r*r;
    aux1 = 0.5 * (x*x+r_2)/(par_b*par_b) - 0.5;
    aux2 = sqrt(aux1*aux1 + r_2/(par_b*par_b));
    tmp1 = sqrt(aux1+aux2)+sqrt(aux1+aux2+1);
    *A = 4*tmp1 / (1+tmp1) - 3;
    tmp2 = sqrt(-aux1+aux2)/
           (1+sqrt(1-(-aux1+aux2)));
    *B = (tmp2-1) / (tmp2+1);
/*
    *B = (sqrt(-aux1+aux2) - 1.-sqrt(1.-(-aux1+aux2))) /
         (sqrt(-aux1+aux2) + 1.+sqrt(1.-(-aux1+aux2)));
*/
/*
    CCTK_REAL b = par_b;
    *B = 
-Sqrt((Power(b,2) + Power(r,2) + Sqrt(Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2)) - 
        Sqrt(2)*Power(x,2)*Sqrt((Power(b,4) + Power(b,2)*
              (2*Power(r,2) - Power(x,2) + Sqrt(Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2)))\
              + Power(r,2)*(Power(r,2) + Power(x,2) + 
                Sqrt(Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))))/Power(x,4)))/Power(x,2));
*/
    if (x<0)
        *B = -*B;
    /* transformation of a scalar for ivar==0 and for a vector for
       ivar in [1,3] */
#define bAB par_b, *A, *B
#define bxr par_b, x, r
    /* data itself does not change */
    tmp_U.d0[0] = U.d0[0];
    /* However, the derivatives do */
    for (int i=1; i<=3; i++)
    {
        /* first, the translation of an scalar */
        tmp_U.d1[0] += J(i,1,bAB)*get_U_(U, i)[0];
        tmp_U.d2[0] += J(i,2,bAB)*get_U_(U, i)[0];
        tmp_U.d3[0] += J(i,3,bAB)*get_U_(U, i)[0];
        for (int j=1; j<=3; j++)
        {
            tmp_U.d11[0] += J(i,1,bAB)*J(j,1,bAB)*get_U__(U,i,j)[0];
            tmp_U.d12[0] += J(i,1,bAB)*J(j,2,bAB)*get_U__(U,i,j)[0];
            tmp_U.d13[0] += J(i,1,bAB)*J(j,3,bAB)*get_U__(U,i,j)[0];
            tmp_U.d22[0] += J(i,2,bAB)*J(j,2,bAB)*get_U__(U,i,j)[0];
            tmp_U.d23[0] += J(i,2,bAB)*J(j,3,bAB)*get_U__(U,i,j)[0];
            tmp_U.d33[0] += J(i,3,bAB)*J(j,3,bAB)*get_U__(U,i,j)[0];
        }
    }
#undef bxr
#undef bAB
    copy_derivs(nvar, tmp_U, U);
    free_derivs(&tmp_U, nvar);
}

void test_transform()
{
    DECLARE_CCTK_PARAMETERS;
    FILE *test;
    test = fopen("test.dat", "w");
    CCTK_REAL x,r,A,B,nx,nr,nA,nB, Am1;
    derivs U;
    derivs Ul;
    int nvar=4;
    int indx[4];
    int n1=60,n2=60,n3=4;
    allocate_derivs(&U, nvar*n1*n2*n3);
    allocate_derivs(&Ul, nvar);
    /* setup test function (only works for scalars) */
    for (int i=0; i<n1; i++)
     for (int j=0; j<n2; j++)
      for (int k=0; k<n3; k++)
      {
        /* get all coordinates of this point */
        A = -cos(Pih * (2 * i + 1) / n1);
        B = -cos(Pih * (2 * j + 1) / n2);
        x = -par_b * (1.+0.25*(A+1.)*(A+1.))/(1.-0.25*(A+1.)*(A+1.)) *
                      2*B    /(1+B*B);
        r =  par_b * (A+1.)                 /(1.-0.25*(A+1.)*(A+1.)) *
                      (1-B*B)/(1+B*B);
        for (int ivar=0; ivar<nvar; ivar++)
        {
            indx[ivar] = Index(ivar, i, j, k, nvar, n1, n2, n3);
            U.d0[indx[ivar]] = exp(-1./par_b/par_b*(r*r+x*x));
//            U.d0[indx[ivar]] = x*exp(-1./100000*(r*r+x*x));
//            U.d0[indx[ivar]] /= A-1.;
        }
      }
    /* get derivatives of testfunction with respect to A,B,phi */
    Derivatives_AB3(nvar, n1, n2, n3, U);
    /* calculate all with respect to x,r,phi */
    for (int i=0; i<n1; i++)
    {
     for (int j=0; j<n2; j++)
      for (int k=0; k<n3; k++)
      {
        /* get all coordinates of this point */
        A = -cos(Pih * (2 * i + 1) / n1);
        Am1 = A-1.;
        Am1 = 1.0;
        B = -cos(Pih * (2 * j + 1) / n2);
        x = -par_b * (1.+0.25*(A+1.)*(A+1.))/(1.-0.25*(A+1.)*(A+1.)) *
                      2*B    /(1+B*B);
        r =  par_b * (A+1.)                 /(1.-0.25*(A+1.)*(A+1.)) *
                      (1-B*B)/(1+B*B);
        /* copy the data of this point to local variable */
        /* plus undo the Am1-thingy */
        for (int ivar=0; ivar<nvar; ivar++)
        {
          indx[ivar] = Index(ivar, i, j, k, nvar, n1, n2, n3);
          Ul.d0[ivar] = Am1 * U.d0[indx[ivar]];
          Ul.d1[ivar] = Am1 * U.d1[indx[ivar]];// + U.d0[indx[ivar]];
          Ul.d2[ivar] = Am1 * U.d2[indx[ivar]];
          Ul.d3[ivar] = Am1 * U.d3[indx[ivar]];
          Ul.d11[ivar]= Am1 * U.d11[indx[ivar]];// + 2 * U.d1[indx[ivar]];
          Ul.d12[ivar]= Am1 * U.d12[indx[ivar]];// +     U.d2[indx[ivar]];
          Ul.d13[ivar]= Am1 * U.d13[indx[ivar]];// +     U.d3[indx[ivar]];
          Ul.d22[ivar]= Am1 * U.d22[indx[ivar]];
          Ul.d23[ivar]= Am1 * U.d23[indx[ivar]];
          Ul.d33[ivar]= Am1 * U.d33[indx[ivar]];
        }
        /* for phi==0, do debug output */
        if (k==0)
        {
          /* initial output */
          fprintf(test, "%.15g %.15g %.15g %15g ", x, r, A, B);
          fprintf(test, "%.15g %.15g %.15g %.15g " /*5*/
                        "%.15g %.15g %.15g %.15g %.15g %.15g ",
              Ul.d0[0], Ul.d1[0],  Ul.d2[0], Ul.d3[0],
              Ul.d11[0], Ul.d12[0], Ul.d13[0],
              Ul.d22[0], Ul.d23[0], Ul.d33[0]);
          fprintf(test, "%.15g %.15g %.15g %.15g ", /*15*/
                        Jm(1,1,par_b,x,r)*Jm(1,1,par_b,x,r)*Ul.d11[0]+
                        Jm(1,1,par_b,x,r)*Jm(2,1,par_b,x,r)*Ul.d12[0]+
                        Jm(2,1,par_b,x,r)*Jm(1,1,par_b,x,r)*Ul.d12[0]+
                        Jm(2,1,par_b,x,r)*Jm(2,1,par_b,x,r)*Ul.d22[0],
                        Jm(1,1,par_b,x,r)*Jm(1,1,par_b,x,r)*Ul.d11[0],
                        Jm(1,1,par_b,x,r)*Jm(2,1,par_b,x,r)*Ul.d12[0],
                        Jm(2,1,par_b,x,r)*Jm(2,1,par_b,x,r)*Ul.d22[0]
                        );
          /* convertion to x,r,phi */
          AB3_To_xr3(1, A, B, &nx, &nr, Ul);
          /* more output */
          fprintf(test, "%.15g %.15g ", nx, nr);
          fprintf(test, "%.15g %.15g %.15g %.15g " /*21*/
                        "%.15g %.15g %.15g %.15g %.15g %.15g ",
              Ul.d0[0], Ul.d1[0],  Ul.d2[0], Ul.d3[0],
              Ul.d11[0], Ul.d12[0], Ul.d13[0],
              Ul.d22[0], Ul.d23[0], Ul.d33[0]);
          /* get nA and nB and calculate the 1st derivatives of the scalar with
             resp. to A and B*/
          Ul.d1[0] = exp(-1./par_b/par_b*(r*r+x*x))*(-2.*x)/par_b/par_b;
          Ul.d2[0] = exp(-1./par_b/par_b*(r*r+x*x))*(-2.*r)/par_b/par_b;
          Ul.d3[0] = 0.0;
          Ul.d11[0] = exp(-1./par_b/par_b*(r*r+x*x))*
                      (4*x*x/par_b/par_b-2)/par_b/par_b;
          Ul.d12[0] = exp(-1./par_b/par_b*(r*r+x*x))*
                      4*x*r/par_b/par_b/par_b/par_b;
          Ul.d22[0] = exp(-1./par_b/par_b*(r*r+x*x))*
                      (4*r*r/par_b/par_b-2)/par_b/par_b;
          Ul.d13[0] = Ul.d23[0] = Ul.d33[0] = 0.0;
          /*
          Ul.d1[0] =  exp(-1./100000*(r*r+x*x))*(1-x*x*2./100000);
          Ul.d2[0] =  exp(-1./100000*(r*r+x*x))*(-r*r*2./100000);
          */
          xr3_To_AB3(1, x, r, &nA, &nB, Ul);
          fprintf(test, "%.15g %.15g ", nA, nB);
          fprintf(test, "%.15g %.15g %.15g %.15g " /*33*/
                        "%.15g %.15g %.15g %.15g %.15g %.15g ",
              Ul.d0[0], Ul.d1[0],  Ul.d2[0], Ul.d3[0],
              Ul.d11[0], Ul.d12[0], Ul.d13[0],
              Ul.d22[0], Ul.d23[0], Ul.d33[0]);
          fprintf(test, "\n");
        }
      }
    fprintf(test, "\n");
    }
    fclose(test);
    free_derivs(&Ul, nvar);
    free_derivs(&U, nvar*n1*n2*n3);
}

